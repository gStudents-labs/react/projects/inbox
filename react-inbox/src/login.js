import React, { useState } from 'react'
import {
  Button,
  Form,
  FormGroup,
  Label,
  Container,
  Row,
  Col,
  Alert,
  Input
} from 'reactstrap'

const Login = () => {


    return (
      <Container className="main-wrapper">
        <Row className="row">
          <Col
            lg={{ size: 6, offset: 3 }}
            className="column"
          >
            <Form>
              <FormGroup>
                <Label for="email-field">Email</Label>
                <Input
                  type="email"
                  name="email"
                  id="email-field"
                  placeholder="email"
                  // value={email}
                  // onChange={e => this.setState({email: e.target.value})}
                />
              </FormGroup>
              <FormGroup>
                <Label for="password-field">Password</Label>
                <Input
                  type="password"
                  name="password"
                  id="pass-field"
                  placeholder="password"
                  // value={password}
                  // onChange={e => this.setState({password: e.target.value})}
                />
              </FormGroup>
              <Button className="mr-3" type="submit" color="primary">
                Submit
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    )
    
}

export default Login;

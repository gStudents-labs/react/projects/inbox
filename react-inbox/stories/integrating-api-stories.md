### Integrating API

Users should be able to persist messages instead of losing changes with every refresh

For this story you will no longer interact with the data from the sample set, you will use the [Collective API](https://gitlab.com/gStudents-labs/react/projects/collective-api). Instructions for how to start and interact with that API are available in the API's repo.

Note: The API is used for multiple projects, for this section please refer to Project: React Inbox in the Read Me. You can also find the code for the routes [here](https://gitlab.com/gStudents-labs/react/projects/collective-api/-/blob/main/app/messages/routes.js)


```
When a user hits the browser refresh button, all existing messages remain visible
When a user hits the browser refresh button, any messages that have been deleted do not reappear.
When a user hits the browser refresh button, all starred messages remain starred and unstarred messages remain unstarred
When a user hits the browser refresh button, all read messages remain read and unread messages remain unread
When a user hits the browser refresh button, all labels assigned to a message remain unchanged
```

### Using React-Router

Users should be required to login before visiting their inbox.

There is starter code available for this [here](https://gitlab.com/gStudents-labs/react/projects/inbox/-/tree/master/react-inbox/src/login.js)

Note: This element uses reactstrap for styling. Therefore instead of HTML you've been given a full Login element. You will still need to add state management, form functionality, and router functionality to the element.

For this story you will use the [Collective API](https://gitlab.com/gStudents-labs/react/projects/collective-api). You are probably already using this API to persist your message data, but instructions for how to start and interact with that API are available in the API's repo.

Note: The API is used for multiple projects, for this section please refer to Router Unit: User Login in the Read Me. You can also find the code for the routes [here](https://gitlab.com/gStudents-labs/react/projects/collective-api/-/blob/main/app/users/routes.js)

## Test Login

```text
Email: carpenterwatts@enerforce.com
Password: hello
```

```
/login should display the Login Component (sample code available here)
/inbox should display the User's Inbox
As a user, when I navigate to `/` the first time, I am automatically redirected to `/login`
As a user, when I click the submit button on the login form and I entered the correct credentials (see Test Login info above), I am sent to `/inbox`
As a user, when I click on the submit button on the login form and I enter incorrect credentials, I stay on /login

```

### Integrating Redux

In this section we are not adding or removing any new functionality. Instead, you will refactor your code to use Redux instead of local state 

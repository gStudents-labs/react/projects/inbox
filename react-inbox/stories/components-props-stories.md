### Message Display

Users should see a list of messages with the correct styles

None of these stories require interactivity (ie, toggling selected or starred). Simply display correctly based on the existing data set.

Use the code found [here](https://gitlab.com/gStudents-labs/react/projects/inbox/-/tree/master/react-inbox/sample-HTML) to start writing your components. You won't necessarily use every single piece of HTML we've provided- some are examples of how your message/toolbar might change as you interact with your app. You'll have to make your code reactive to users' interactions, but use this as a style guide to determine how your page should respond.

There is sample data available [here](https://gitlab.com/gStudents-labs/react/projects/inbox/-/tree/master/react-inbox/src/sample-data)

### Component Hierarchy

Normally you get to decide how to organize components when you create an app.  For this app, make sure have:

- An App component
- A Messages (or MessageList) component
- A Message component
- A Toolbar component


```
When a user views the app:
- They should see a list of messages with their subjects
- They should see a toolbar at the top of the page
- If a message is read, it should have the read style
- If a message is unread, it should have the unread
- If a message is selected, it should have the selected style and the box should be checked
- If there are labels on a message, they should appear
- If a message is starred, then the star should be filled in, otherwise it should be empty
```

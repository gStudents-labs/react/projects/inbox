# React Inbox

Welcome to the React Inbox! In this project, you'll be building a gMail inbox clone. This is an iterative project- in each section you'll apply the new skills and technologies you've learned to add and refine the functionality of your inbox app.

## Getting started

This repo has a directory, react-inbox, that includes a lightly edited create-react-app project to get you started. It also includes all the CSS and styling you'll need. Each section below has stories to guide your work and let you know what functionality you need to build into your app. You will have to create multiple components to accomplish this. We've included sample HTML code you can use as a style guide to see how your code will react to interaction and state changes- you should appropriately convert that HTML into JSX and use it in the various components you'll need to build.

### To start:

Clone this repo - it includes all the starter React and style code you'll need.

```
npm install
npm start
```

Click the appropriate section below to find user stories that will guide your work.

## Sections

-  [Components/Props](https://gitlab.com/gStudents-labs/react/projects/inbox/-/blob/master/stories/components-props-stories.md)
-  [State/Hooks](https://gitlab.com/gStudents-labs/react/projects/inbox/-/blob/master/stories/state-hooks.md)
-  [Integrate API](https://gitlab.com/gStudents-labs/react/projects/inbox/-/blob/master/stories/integrating-api-stories.md)
-  [Redux](https://gitlab.com/gStudents-labs/react/projects/inbox/-/blob/master/stories/redux-stories.md)
-  [Router](https://gitlab.com/gStudents-labs/react/projects/inbox/-/blob/master/stories/router-stories.md)
